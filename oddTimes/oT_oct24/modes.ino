void m0(){
  now=millis();
  if(pcount==0)clkDiv=map(analogRead(9),0,1024,8,17);
  barTime=now-lastBar;
  stepTime=now-lastStep;
  maxGateLength=((analogRead(currentStep)*pulseInterval)/1024);
  checkGateLenghts();

  outputGates();
  if(pulse==HIGH){
    dive++;
    pulseInterval=now-lastPulse;
    lastPulse=now;    
    pulse=LOW;
    pcount++;
    if(pcount>clkDiv-1){
      pcount=0;
      lastBar=now;
    }


    if(dive>=map(analogRead(9),0,1024,1,9)){
      dive=0;
      if(!digitalRead(22))currentStep<stepsAmount-1 ? currentStep++ : currentStep=0 ;   
      if(analogRead(currentStep)<10){
        if(!digitalRead(22))currentStep<stepsAmount-1 ? currentStep++ : currentStep=0 ; 
        for(int i=currentStep;i<stepsAmount;i++){
          if(analogRead(i)<10)currentStep > stepsAmount-2 ? currentStep=0 : currentStep++ ; 
        }
      }
      lastStep=now;
      if(!digitalRead(22))triggerGates(currentStep); 

      for(int i=0;i<stepsAmount;i++)i==currentStep ? digitalWrite(i+2,HIGH) : digitalWrite(i+2,LOW) ;

      if(analogRead(currentStep)>1010)currentStep=stepsAmount-1;
    }

  }

}

void m0b(){
  now=millis();
  if(pcount==0)clkDiv=map(analogRead(9),0,1024,8,17);
  if(!digitalRead(22)){
    barTime=now-lastBar;  
    stepTime=now-lastStep;


    if(stepTime>=barLength/8&&readings[currentStep][2]==0){
      readings[currentStep][2]=1;
      lastStep=now;
  
      if(currentStep<stepsAmount-1){
        currentStep++; 
      }
      else{
        currentStep=0;
        for(int i=0;i<stepsAmount;i++)readings[i][2]=0;
      }    

      triggerGates(currentStep); 
      
    }


    maxGateLength=readings[currentStep][0]*0.5;
    checkGateLenghts();

    for(int i=0;i<stepsAmount;i++)i==currentStep ? digitalWrite(i+2,HIGH) : digitalWrite(i+2,LOW) ;

    outputGates();
  }

  if(pulse==HIGH){
    pulseInterval=now-lastPulse;
    lastPulse=now;    
    pulse=LOW;
    if(!digitalRead(22))pcount++;
    if(pcount>clkDiv-1){
      pcount=0;
      if(!digitalRead(22)){
        lastBar=now;     
        lastStep=now;
        currentStep=0;
        for(int i=0;i<stepsAmount;i++)readings[i][2]=0;
        triggerGates(currentStep); 
      }
    } 

    barLength=pulseInterval*clkDiv;
    readingsTotal=0;
    for(int i=0;i<stepsAmount;i++){
      readings[i][0]=map(analogRead(i),0,1024,8,32);
      readings[i][1]=i;
      readingsTotal+=readings[i][0];        
    }
    for(int i=0;i<stepsAmount;i++){
      readings[i][0]=(barLength*readings[i][0])/readingsTotal;  
    }
  if(analogRead(currentStep)>1010){
        pcount=0;
      if(!digitalRead(22)){
        lastBar=now;     
        lastStep=now;
        currentStep=0;
        for(int i=0;i<stepsAmount;i++)readings[i][2]=0;
        triggerGates(currentStep); 
      }
  }
  }


}


void m1(){
  now=millis();
  if(pcount==0)clkDiv=map(analogRead(9),0,1024,8,17);
  if(!digitalRead(22)){
    barTime=now-lastBar;  
    stepTime=now-lastStep;


    if(stepTime>=readings[currentStep][0]&&readings[currentStep][2]==0){
      readings[currentStep][2]=1;
      lastStep=now;

      if(currentStep<stepsAmount-1){
        currentStep++; 
      }
      else{
        currentStep=0;
        for(int i=0;i<stepsAmount;i++)readings[i][2]=0;
      }    

      triggerGates(currentStep); 
    }


    maxGateLength=readings[currentStep][0]*0.5;
    checkGateLenghts();

    for(int i=0;i<stepsAmount;i++)i==currentStep ? digitalWrite(i+2,HIGH) : digitalWrite(i+2,LOW) ;

    outputGates();
  }

  if(pulse==HIGH){
    pulseInterval=now-lastPulse;
    lastPulse=now;    
    pulse=LOW;
    if(!digitalRead(22))pcount++;
    if(pcount>clkDiv-1){
      pcount=0;
      if(!digitalRead(22)){
        lastBar=now;     
        lastStep=now;
        currentStep=0;
        for(int i=0;i<stepsAmount;i++)readings[i][2]=0;
        triggerGates(currentStep); 
      }
    } 

    barLength=pulseInterval*clkDiv;
    readingsTotal=0;
    for(int i=0;i<stepsAmount;i++){
      readings[i][0]=map(analogRead(i),0,1024,8,32);
      readings[i][1]=i;
      readingsTotal+=readings[i][0];        
    }
    for(int i=0;i<stepsAmount;i++){
      readings[i][0]=(barLength*readings[i][0])/readingsTotal;  
    }
    
  }

}


void m3(){
  now=millis();
  barTime=now-lastBar;
  stepTime=now-lastStep;


  if(barTime>=readings[currentStep][0]&&readings[currentStep][2]==0){
    //lastStep=now;
    readings[currentStep][2]=1;
    maxGateLength=readings[currentStep][0]*0.5;
    currentStep < stepsAmount-1 ? currentStep++ : currentStep=0 ;

    checkGateLenghts();

    triggerGates(currentStep);

  }




  for(int i=0;i<stepsAmount;i++)i==readings[currentStep][1] ? digitalWrite(i+2,HIGH) : digitalWrite(i+2,LOW) ;  

  outputGates();


  if(pulse==HIGH){
    pulseInterval=now-lastPulse;
    lastPulse=now;    
    barLength=8*pulseInterval;
    pulse=LOW;
    pcount++;

    for(int i=0;i<stepsAmount;i++){

      readings[i][0]=map(analogRead(i),0,1024,pulseInterval/2,barLength-1);

      readings[i][1]=i;//: readings[i][1]=0;

    }
    insertion_srt();

    //Serial.println(currentStep);

    if(pcount>7){
      pcount=0;
      lastBar=now;
      for(int i=0;i<stepsAmount;i++)readings[i][2]=0;
      //currentStep=0;//readings[0][1];
    }

  }


}

void m2(){
  now=millis();
  barTime=now-lastBar;
  stepTime=now-lastStep;

  maxGateLength=readings[currentStep][0];

  for(int i=0;i<stepsAmount;i++)i==currentStep ? digitalWrite(i+2,HIGH) : digitalWrite(i+2,LOW) ;



  checkGateLenghts();




  outputGates();



  if(pulse==HIGH){
    dive++;
    pulseInterval=now-lastPulse;
    lastPulse=now;    
    pulse=LOW;
    pcount++;

    barLength=0;
    for(int i=0;i<stepsAmount;i++){

      readings[i][0]=map(analogRead(i),0,1024,1,16)*pulseInterval/2;

      readings[i][1]=i;

      barLength+=readings[i][0];

    }


  }



  if(stepTime>=readings[currentStep][0]&&readings[currentStep][2]==0){
    readings[currentStep][2]=1;
    lastStep=now;

    if(currentStep<stepsAmount-1){
      currentStep++; 
    }
    else{
      currentStep=0;
      for(int i=0;i<stepsAmount;i++)readings[i][2]=0;
      pcount=0;
    }    

    triggerGates(currentStep); 
  }



}






void m4(){
  now=millis();
  barTime=now-lastBar;
  if(digitalRead(22)){
    play=true;
    pcount=0;
    currentStep=stepsAmount-1;
  }
  if(pulse==HIGH){
    dive++;
    pulseInterval=now-lastPulse;
    lastPulse=now;    
    pulse=LOW;
    pcount++;
    if(pcount>8){
      pcount=0;
      lastBar=now;
      play=false;
    }

    if(dive>=map(analogRead(9),0,1024,1,9)){
      dive=0;
      if(play){
        currentStep > stepsAmount-2 ? currentStep=0 : currentStep++ ;   
        if(analogRead(currentStep)<10){
          currentStep > stepsAmount-2 ? currentStep=0 : currentStep++ ; 
          for(int i=currentStep;i<stepsAmount;i++){
            if(analogRead(i)<10)currentStep > stepsAmount-2 ? currentStep=0 : currentStep++ ; 
          }
        }


        maxGateLength=((analogRead(currentStep)*pulseInterval)/1024);
        triggerGates(currentStep); 
        for(int i=0;i<stepsAmount;i++)i==currentStep ? digitalWrite(i+2,HIGH) : digitalWrite(i+2,LOW) ;



        checkGateLenghts();
        if(analogRead(currentStep)>1010)currentStep=stepsAmount-1;
      }
    }
  }

  outputGates();

}

















