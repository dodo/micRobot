void pulseIN(){
  pulse=HIGH;
}



void checkGateLenghts(){
  gate1Length=map(analogRead(14),0,1024, 5,maxGateLength);
  gate2Length=map(analogRead(15),0,1024,5, maxGateLength);
}


void outputGates(){
  if(now-lastG1Trigger>=gate1Length) gate1state=LOW;
  if(now-lastG2Trigger>=gate2Length) gate2state=LOW;
  digitalWrite(12, gate1state);
  digitalWrite(13, gate2state); 
}

void triggerGates(int cstep){

  if(digitalRead(23+cstep*2)==HIGH||(digitalRead(23+cstep*2)==LOW&&digitalRead(39+cstep*2)==LOW)){
    if(gate1state==LOW){
      lastG1Trigger=millis();
      gate1state=HIGH;
    }
  }
  if(digitalRead(39+cstep*2)==HIGH||(digitalRead(23+cstep*2)==LOW&&digitalRead(39+cstep*2)==LOW)){
    if(gate2state==LOW){
       lastG2Trigger=millis();
      gate2state=HIGH;
    }
  } 


}

int mode(){ // read the mode pot value and pass it to the loop + indicate the current mode with leds;

  if(pcount==0)currentMode=map(analogRead(8),0,1000,6,0);
  
  if(map(analogRead(8),0,1000,6,0)!=currentMode){
    if(blinking>40){
     blinking=0;
     tempMode<5?tempMode++:tempMode=0;
       switch(tempMode){

  case 0:
    digitalWrite(24, HIGH);
    digitalWrite(26, LOW);
    digitalWrite(28, LOW);
    break;

  case 1:
    digitalWrite(24, HIGH);
    digitalWrite(26, HIGH);
    digitalWrite(28, LOW);
    break;

  case 2:
    digitalWrite(24, LOW);
    digitalWrite(26, HIGH);
    digitalWrite(28, LOW);
    break;

  case 3:
    digitalWrite(24, LOW);
    digitalWrite(26, HIGH);
    digitalWrite(28, HIGH);
    break;

  case 4:
    digitalWrite(24, LOW);
    digitalWrite(26, LOW);
    digitalWrite(28, HIGH);
    break;

  case 5:
    digitalWrite(24, HIGH);
    digitalWrite(26, LOW);
    digitalWrite(28, HIGH);
    break;


  } 
    }
    blinking++;
  }
  
  else{
  
  switch(currentMode){

  case 0:
    digitalWrite(24, HIGH);
    digitalWrite(26, LOW);
    digitalWrite(28, LOW);
    break;

  case 1:
    digitalWrite(24, HIGH);
    digitalWrite(26, HIGH);
    digitalWrite(28, LOW);
    break;

  case 2:
    digitalWrite(24, LOW);
    digitalWrite(26, HIGH);
    digitalWrite(28, LOW);
    break;

  case 3:
    digitalWrite(24, LOW);
    digitalWrite(26, HIGH);
    digitalWrite(28, HIGH);
    break;

  case 4:
    digitalWrite(24, LOW);
    digitalWrite(26, LOW);
    digitalWrite(28, HIGH);
    break;

  case 5:
    digitalWrite(24, HIGH);
    digitalWrite(26, LOW);
    digitalWrite(28, HIGH);
    break;


  } 
  } 
  return currentMode;
}

 void insertion_srt(){
  for (int i = 1; i < stepsAmount; i++){
  int j = i;
  int B = readings[i][0];
  int C = readings[i][1];
  while ((j > 0) && (readings[j-1][0] > B)){
  readings[j][0] = readings[j-1][0];
 readings[j][1] = readings[j-1][1];
  j--;
  }
  readings[j][0] = B;
  readings[j][1]=C;
  }
  }

