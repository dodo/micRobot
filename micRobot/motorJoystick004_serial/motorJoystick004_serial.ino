#define MIC 0
#define ARM 1
#define TURN 2

#define M01ENBL  11
#define M02ENBL  10
#define M03ENBL  9
#define M01T1 12
#define M01T2 13
#define M02T1 6
#define M02T2 7
#define M03T1 4
#define M03T2 5
#define JUD A0
#define JLR A1
#define POT A3

int sData[] = {512, 512};
int mData[3][3] = {{JUD, 0, 1}, {JLR, 0, 1}, {POT, 0, 1}};
int mOut[3][3] = {{M01ENBL, M01T1, M01T2}, {M02ENBL, M02T1, M02T2}, {M03ENBL, M03T1, M03T2}};

void setup()
{
  Serial.begin(9600);
  pinMode(M01ENBL, OUTPUT);
  pinMode(M02ENBL, OUTPUT);
  pinMode(M03ENBL, OUTPUT);
  pinMode(M01T1, OUTPUT);
  pinMode(M01T2, OUTPUT);
  pinMode(M02T1, OUTPUT);
  pinMode(M02T2, OUTPUT);
  pinMode(M03T1, OUTPUT);
  pinMode(M03T2, OUTPUT);

  digitalWrite(M01ENBL, LOW);
  digitalWrite(M02ENBL, LOW);
  digitalWrite(M03ENBL, LOW);
  digitalWrite(M01T1, LOW);
  digitalWrite(M01T2, LOW);
  digitalWrite(M02T1, LOW);
  digitalWrite(M02T2, LOW);
  digitalWrite(M03T1, LOW);
  digitalWrite(M03T2, LOW);

  pinMode(3, OUTPUT);

}

void loop()
{
  while (Serial.available() > 0) {
    sData[0] = Serial.parseInt();
    sData[1] = Serial.parseInt();
    getJvalueMapped(MIC);
    runMotor(MIC);
    getJvalueMapped(ARM);
    runMotor(ARM);
  }


}



void getJvalueMapped(int whichMotor ) {
  int reading = sData[whichMotor] * 4;
  int value = 0;
  if (reading > 530) {
    value = map(reading, 512, 1024, 0, 255);
    mData[whichMotor][1] = value;
    mData[whichMotor][2] = 1;
  }
  else if (reading < 494) {
    value = map(reading, 0, 512, 255, 0);
    mData[whichMotor][1] = value;
    mData[whichMotor][2] = 0;
  }
  else {
    mData[whichMotor][1] = 0;
    mData[whichMotor][2] = 0;
  }
}

void runMotor(int whichMotor) {
  analogWrite(mOut[whichMotor][0], mData[whichMotor][1]);
  digitalWrite(mOut[whichMotor][1], 1 - mData[whichMotor][2]);
  digitalWrite(mOut[whichMotor][2], mData[whichMotor][2]);
}
